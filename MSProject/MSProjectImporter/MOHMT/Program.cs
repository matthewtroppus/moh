﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using net.sf.mpxj;
using net.sf.mpxj.MpxjUtilities;
using net.sf.mpxj.reader;
using REX.DAL.MOHTMT;
using System.Data.SqlClient;

namespace MOHTMTImport
{
    public class Importer
    {

        public struct WorkingHours
        {
            public DateTime StartDate;
            public DateTime EndDate;
            public int Hours;
            


            public WorkingHours(DateTime startDate, DateTime endDate, int hours)
            {
                EndDate = endDate;
                StartDate = startDate;
                Hours = hours;
            }
        }


        public static Boolean ImportFile(string fileName)
        {
            try
            {
                ProjectReader reader = ProjectReaderUtility.getProjectReader(fileName);
                ProjectFile mpxFile = reader.read(fileName);
                UpdateDB(mpxFile);
                return true;                
            }
            catch (Exception)
            {
                throw;
                return false;
            }
        }

        private static void UpdateDB(ProjectFile mpxFile)
        {
            try
            {
                //System.Console.WriteLine("Filename | Work Order Number |" + " Op No | " + "Operation Description | " + "Start Date | " + "End Date | "
                //                    + "Duration |" + "Resource |" + "Qty | " + "Unique |" + "FilePath");
                //System.Diagnostics.Debug.WriteLine("Filename | Work Order Number |" + " Op No | " + "Operation Description | " + "Start Date | " + "End Date | "
                //                        + "Duration |" + "Resource |" + "Qty | " + "Unique |" + "FilePath\r\n");

                List<WorkingHours> scheduledHours = new List<WorkingHours>();
                String workOrderNumber;
                String startDate;
                String finishDate;
                int duration;
                Duration dur;
                String opNumber;


                foreach (net.sf.mpxj.Task task in mpxFile.AllTasks.ToIEnumerable())
                {
                    var obj = task.getFieldByAlias("Work Order No.");
                    if (obj != null)
                    {
                        workOrderNumber = obj.ToString();
                    }
                    else
                    {
                        workOrderNumber = "";
                    }

                    if (ExistingRecords(workOrderNumber))
                        RemoveRecords(workOrderNumber);
                }

                foreach (net.sf.mpxj.Task task in mpxFile.AllTasks.ToIEnumerable())
                {
                    String fileName = "";

                    var obj = task.getFieldByAlias("Work Order No.");
                    if (obj != null)
                    {
                        workOrderNumber = obj.ToString();
                    }
                    else
                    {
                        workOrderNumber = "";
                    }

                    obj = task.getFieldByAlias("Operation No.");
                    if (obj != null)
                    {
                        opNumber = obj.ToString();
                    }
                    else
                    {
                        opNumber = "";
                    }

                    if (string.IsNullOrEmpty(workOrderNumber) ||
                        string.IsNullOrEmpty(opNumber))
                    {
                        continue;
                    }

                    if (task.ResourceAssignments.isEmpty())
                    {
                        continue;
                    }

                    //if (task.Name != "Remove airslide covers")
                    ////if (task.Name != "Housekeeping")
                    //{
                    //    continue;
                    //}

                    var date = task.Start;
                    if (date != null)
                    {
                        startDate = date.ToDateTime().ToString();
                    }
                    else
                    {
                        startDate = "(no date supplied)";
                    }

                    date = task.Finish;
                    if (date != null)
                    {
                        finishDate = date.ToDateTime().ToString();
                    }
                    else
                    {
                        finishDate = "(no date supplied)";
                    }

                    dur = task.Duration;
                    if (dur != null)
                    {
                        duration = Convert.ToInt16(dur.Duration) * 60;
                    }
                    else
                    {
                        duration = 0;
                    }

                    if (mpxFile.ProjectProperties.ProjectFilePath.Length == 0)
                    {
                        AlertNoFileName();
                    }
                    else
                    {
                        string temp = mpxFile.ProjectProperties.ProjectFilePath;
                        fileName = temp.Substring(temp.LastIndexOf("\\") + 1).Split('-')[0];
                    }

                    net.sf.mpxj.ProjectCalendarContainer allCalendars = mpxFile.Calendars;

                    if (task.ResourceAssignments != null)
                    {
                        var res = task.ResourceAssignments.ToIEnumerable();
                        var uniqueId = string.Format("{0}-{1}", task.GetFieldByAlias("Work Order No."), task.GetFieldByAlias("Operation No."));



                        //****** Uncomment these lines and comment the Model Update lines to debug ////
                        //foreach (ResourceAssignment r in res)
                        //{
                        //    ProjectCalendar myCalendar = allCalendars.GetByName(r.Resource.Name);
                        //    string calName = myCalendar.GetParent().Name;
                        //    scheduledHours = CalculateHours(r);

                        //    foreach (WorkingHours wh in scheduledHours)
                        //    {
                        //        var units = r.Units.ToNullableInt() / 100;

                        //        string detailLine = String.Format("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10} | {11}",
                        //                        fileName,
                        //                        workOrderNumber,
                        //                        opNumber,
                        //                        task.Name,
                        //                        wh.StartDate,
                        //                        wh.EndDate,
                        //                        wh.Hours * 60,// * units,
                        //                        r.Resource.Name,
                        //                        units,
                        //                        uniqueId,
                        //                        mpxFile.ProjectProperties.ProjectFilePath,
                        //                        Environment.UserName);
                        //        System.Console.WriteLine(detailLine);
                        //        System.Console.WriteLine(Environment.NewLine);
                        //        System.Diagnostics.Debug.WriteLine(detailLine + "\r\n");

                        //    }
                        //}
                        //**************************************///


                        MohtmtEntities ctx = new MohtmtEntities();
                        try
                        {
                            foreach (ResourceAssignment resource in res)
                            {
                                ProjectCalendar myCalendar = task.Calendar;
                                if(myCalendar == null)
                                    myCalendar = allCalendars.GetByName(resource.Resource.Name);
                                //string calName = myCalendar.GetParent().Name;
                                scheduledHours = CalculateHours(resource, myCalendar);

                                foreach (WorkingHours wh in scheduledHours)
                                {

                                    MOHTMT_MohData MoHline = new MOHTMT_MohData()
                                    {
                                        Filename = fileName,
                                        Work_Order_Number = workOrderNumber,
                                        Op_No = Convert.ToInt16(opNumber),
                                        Operation_Description = task.Name,
                                        Start_Date = wh.StartDate,
                                        End_Date = wh.EndDate,
                                        Duration = wh.Hours * 60,
                                        Resource = resource.Resource.Name,
                                        Quantity = resource.Units.ToNullableInt() / 100,
                                        Unique = uniqueId,
                                        Filepath = mpxFile.ProjectProperties.ProjectFilePath,
                                        Updated_by = Environment.UserName,
                                        CreatedDate = DateTime.Now
                                    };

                                    ctx.MOHTMT_MohDatas.Add(MoHline);

                                    ctx.SaveChanges();

                                }

                            }
                        }
                        catch (Exception ex1)
                        {

                        }
                        finally
                        {
                            if (ctx != null)
                                ctx.Dispose();
                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static void RemoveRecords(string WONumber)
        {
            MohtmtEntities ctx = new MohtmtEntities();
            string sql = String.Format(@"DELETE FROM MOHTMT.MohData WHERE [Work Order Number] = '{0}'", WONumber);
            try
            {
                var p = ctx.Database.ExecuteSqlCommand(sql);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (ctx != null)
                    ctx.Dispose();
            }

        }

        private static bool ExistingRecords(string WONumber)
        {
            bool recordsExist = false;
            MohtmtEntities ctx = new MohtmtEntities();
            try
            {
                var x = ctx.MOHTMT_MohDatas
                     .Where(s => s.Work_Order_Number == WONumber)
                     .Count();

                recordsExist = x == 0 ? false : true;
            }
            finally
            {
                if (ctx != null)
                    ctx.Dispose();
            }
            return recordsExist;
        }

        private static void AlertNoFileName()
        {
            throw new NotImplementedException();
        }


        static List<WorkingHours> CalculateHours(net.sf.mpxj.ResourceAssignment aResource, ProjectCalendar aCalendar)
        {
            List<WorkingHours> hoursToWork = new List<WorkingHours>();

            int i;
            // Dictionary<DateTime, Dictionary<string,string>> workingDay = new Dictionary<DateTime,Dictionary<string,string>>();
            DateTime startDate = aResource.Task.Start.ToDateTime();
            DateTime endDate = aResource.Task.Finish.ToDateTime();

            if (aCalendar.Name == "10.3 CALENDAR")
            {
                DateTime calStartTime;
                DateTime calEndTime;
                DateTime tStartDate, tEndDate, tDate;
                int tHours;
                var year = 1900;
                var month = 01;
                var day = 01;


                var tempStart = aCalendar.GetStartTime(aResource.Task.Start);
                var tempEnd = aCalendar.GetFinishTime(aResource.Task.Finish);


                calStartTime = new DateTime(year, month, day, tempStart.getHours(), tempStart.getMinutes(), tempStart.getSeconds());
                calEndTime = new DateTime(year, month, day, tempEnd.getHours(), tempEnd.getMinutes(), tempEnd.getSeconds());

                DateTime currentDate = startDate.Date;

                do
                {
                    if (IsWorkingDay(aCalendar, currentDate))
                    {

                        if (currentDate == startDate.Date)
                        {
                            tStartDate = startDate;

                        }
                        else
                        {
                            tStartDate = currentDate + new TimeSpan(calStartTime.Hour, 0, 0);
                        }

                        if (currentDate == endDate.Date)
                        {
                            tEndDate = endDate;
                        }
                        else
                        {
                            tEndDate = currentDate + new TimeSpan(calEndTime.Hour, 0, 0);
                        }

                        hoursToWork.Add(new WorkingHours(tStartDate, tEndDate, tEndDate.Subtract(tStartDate).Hours));
                    }

                    currentDate = currentDate.AddDays(1);

                } while (currentDate <= endDate.Date);

            }
            if (aCalendar.Name == "24 Hours")
            {
                int hours = endDate.Subtract(startDate).Hours;
                hoursToWork.Add(new WorkingHours(startDate, endDate, hours));
            }
            return hoursToWork;
        }

        private static Boolean IsWorkingDay(ProjectCalendar aCalendar, DateTime tDate)
        {
            string day = tDate.DayOfWeek.ToString().ToUpper();
            return aCalendar.IsWorkingDay(Day.valueOf(day));
        }

        

    }
}
