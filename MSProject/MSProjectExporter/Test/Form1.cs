﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using MOHTMTImport;


namespace Test
{
    public partial class frmExporter : Form
    {

        private Process mpProcess;
        private Boolean fileImported;

        private const string PROJECTFILEPATH = @"C:\Program Files (x86)\Microsoft Office\Office15\winPROJ.EXE";
        public frmExporter()
        {
            InitializeComponent();
            //Center form to Primary Screeen
            var pointX = (Screen.PrimaryScreen.Bounds.Width / 2) - (this.Width / 2);
            this.Location = new Point(pointX, 0);
            mpProcess = new Process();
            
            
        }

        void mpProcess_Exited(object sender, EventArgs e)
        {
            Boolean success = false;
            try
            {
                DialogResult res = MessageBox.Show("Do you want to Export project data?", "Export Project Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {
                    ExportFile(dlgOpenFile.FileName);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Error Exporting file", "Exporting Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            Boolean success = false;
            if (!fileImported)
            {
                try
                {
                    DialogResult res = MessageBox.Show("Do you want to Export project data?", "Export Project Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.Yes)
                    {
                        ExportFile(dlgOpenFile.FileName);
                    }
                }
                catch (Exception)
                {

                    MessageBox.Show("Error Export file", "Exporting Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            mpProcess.Close();
            mpProcess.Dispose();
            this.Close();
        }

        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            DialogResult res = dlgOpenFile.ShowDialog();
            if (res != DialogResult.Cancel)
            {
                OpenProjectFile(dlgOpenFile.FileName);
                cmdExport.Enabled = true;
                cmdBrowse.Enabled = false;
                fileImported = false;
            }
            else
                fileImported=true;
        }

        private void OpenProjectFile(string fileName)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = PROJECTFILEPATH;
            processStartInfo.Arguments = fileName;
            mpProcess = Process.Start(processStartInfo);
            mpProcess.EnableRaisingEvents = true;
            mpProcess.Exited += new EventHandler(mpProcess_Exited);
        }

        private void cmdExport_Click(object sender, EventArgs e)
        {
            ExportFile(dlgOpenFile.FileName);
            cmdBrowse.Enabled = true;
            cmdExport.Enabled = false;
        }

        private Boolean ExportFile(string fileName)
        {
            Boolean success = false;

            success = MOHTMTImport.Importer.ImportFile(fileName);
            if (success)
            {
                MessageBox.Show("File successfully exported", "Export File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                fileImported = true;
            }

            return success;
        }
    }
}
